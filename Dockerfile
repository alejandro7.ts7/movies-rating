FROM node:16-alpine

RUN mkdir -p /app

ARG CACHEBUST=1

WORKDIR /app

COPY . .

RUN npm install -g pnpm

RUN pnpm install

RUN pnpm run build

EXPOSE 3000

CMD [ "pnpm", "run", "dev" ]
