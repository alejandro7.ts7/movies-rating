"use client"

import { createGlobalStyle } from "styled-components";


export const GlobalStyles = createGlobalStyle`
  *,
  *::before,
  *::after {
    margin: 0;
    padding: 0;
    box-sizing: border-box
  }

  body {
    background-color: #373a3c;
    color: hsl(0, 1%, 16%);
    overflow-x: hidden;
  }
`
