"use client"

import { Button } from 'primereact/button';
import { Rating } from 'primereact/rating';
import { Tag } from 'primereact/tag';
import { DataView } from 'primereact/dataview';
import { useMovies } from "@/hooks/useMovies";
import { Movie } from '@/definations/interfaces/movies';


export default function MoviesList () {
  
  // @ts-ignore
  const { movies } = useMovies();

  const itemTemplate = (movie: Movie) => {
        return (
            <div className="col-12">
                <div className="flex flex-column xl:flex-row xl:align-items-start p-4 gap-4">
                    <img className="w-9 sm:w-16rem xl:w-10rem shadow-2 block xl:block mx-auto border-round" src={`/images/movie_wallpapper.jpg`} alt={movie.title} />
                    <div className="flex flex-column sm:flex-row justify-content-between align-items-center xl:align-items-start flex-1 gap-4">
                        <div className="flex flex-column align-items-center sm:align-items-start gap-3">
                            <div className="text-2xl font-bold text-900">{movie.title}</div>
                            <Rating value={movie.rating_average} readOnly cancel={false}></Rating>
                            <div className="flex align-items-center gap-3">
                                <span className="flex align-items-center gap-2">
                                    <i className="pi pi-copy"></i>
                                    <span className="font-semibold">{movie.genre_name}</span>
                                </span>
                                <span>{movie.plot}</span>
                            </div>
                        </div>
                        <div className="flex sm:flex-column align-items-center sm:align-items-end gap-3 sm:gap-2">
                            <Button icon="pi pi-plus" className="p-button-rounded"></Button>
                        </div>
                    </div>
                </div>
            </div>
        );
    };

    return (
        <div className="card">
            <DataView value={movies} itemTemplate={itemTemplate} />
        </div>
    )
}
