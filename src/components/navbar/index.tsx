"use client"
import { mainMenu } from '@/definations/concepts';
import { Menubar } from 'primereact/menubar';


export default function Navbar() {
  return (
    <div className="card">
      <Menubar model={mainMenu} />
    </div>
  );
}
