import { BearerTokenInterface } from "@/definations/interfaces/auth";
import { fetchWithToken } from "./fetch";
import { ADD_MOVIE, REMOVE_MOVIE, WATCHLIST } from "@/definations/endpoints";
import { Methods } from "@/definations/interfaces/fetching";


export const get = async ({
  token
}: BearerTokenInterface) => {
  return await fetchWithToken({
    url: WATCHLIST,
    params: {},
    method: Methods.GET,
    bearerToken: token,
  });
};


export const create = async({
  watchlist,
  token,
}: BearerTokenInterface): Promise<number> => {
  const response: Response = await fetchWithToken({
    url: ADD_MOVIE,
    params: {},
    method: Methods.POST,
    data: watchlist,
    bearerToken: token,
  });

  return response.status;
};

export const destroy = async({
  watchlist,
  token,
}: BearerTokenInterface): Promise<number> => {
  const response: Response = await fetchWithToken({
    url: REMOVE_MOVIE(watchlist?.id),
    params: {},
    method: Methods.DELETE,
    bearerToken: token,
  });

  return response.status;
};
