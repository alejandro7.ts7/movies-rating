import { AuthPayload } from "@/definations/interfaces/auth";
import fetcWithoutToken from "./fetch";
import { SIGININ, SIGNUP } from "@/definations/endpoints";
import { Methods } from "@/definations/interfaces/fetching";
import { fetchWithToken } from "./fetch";
import { RegisterUserInterface } from "@/definations/interfaces/users";

export const getAuthToken = async ({ username, password }: AuthPayload) => {
  return await fetcWithoutToken({ 
    url: SIGININ, 
    params: {}, 
    method: Methods.POST, 
    data: {
      'username': username,
      'password': password,
  }})
};


export const registerUser = async ({ 
  username, 
  email, 
  firstName, 
  lastName, 
  passwordOne, 
  passwordTwo,
  token,
}: RegisterUserInterface) => {

  return await fetchWithToken({
    url: SIGNUP,
    params: {},
    method: Methods.POST,
    data: {
      "username": username,
      "email": email,
      "first_name": firstName,
      "last_name": lastName,
      "password": passwordOne,
      "password2": passwordTwo,
    },
    bearerToken: token,
  });
}
