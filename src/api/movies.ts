import { BearerTokenInterface } from "@/definations/interfaces/auth";
import fetcWithoutToken, { fetchWithToken } from "./fetch";
import { CREATE_MOVIE, DELETE_MOVIE, MOVIES, MOVIE_BY_ID, UPDATE_MOVIE } from "@/definations/endpoints";
import { Methods } from "@/definations/interfaces/fetching";


export const get = async () => {
  return await fetcWithoutToken({
    url: MOVIES,
    params: {},
    method: Methods.GET,
  });
};


export const getById = async ({
  movie,
  token
}: BearerTokenInterface) => {
  return await fetchWithToken({
    url: MOVIE_BY_ID(movie?.id),
    params: {},
    method: Methods.GET,
    bearerToken: token,
  });
};


export const create = async({
  movie,
  token,
}: BearerTokenInterface): Promise<number> => {
  const response: Response = await fetchWithToken({
    url: CREATE_MOVIE,
    params: {},
    method: Methods.POST,
    data: movie,
    bearerToken: token,
  });

  return response.status;
};

export const update = async({
  movie,
  token,
}: BearerTokenInterface): Promise<number> => {
  const response: Response = await fetchWithToken({
    url: UPDATE_MOVIE(movie?.id),
    params: {},
    method: Methods.PUT,
    data: movie,
    bearerToken: token,
  });

  return response.status;
};


export const destroy = async({
  movie,
  token,
}: BearerTokenInterface): Promise<number> => {
  const response: Response = await fetchWithToken({
    url: DELETE_MOVIE(movie?.id),
    params: {},
    method: Methods.DELETE,
    bearerToken: token,
  });

  return response.status;
};
