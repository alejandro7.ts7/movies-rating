import { BearerTokenInterface } from "@/definations/interfaces/auth";
import { fetchWithToken } from "./fetch";
import { CREATE_RATING, DELETE_RATING, RATINGS, RATING_BY_ID, UPDATE_RATING } from "@/definations/endpoints";
import { Methods } from "@/definations/interfaces/fetching";


export const get = async ({
  token
}: BearerTokenInterface) => {
  return await fetchWithToken({
    url: RATINGS,
    params: {},
    method: Methods.GET,
    bearerToken: token,
  });
};


export const getById = async ({
  rating,
  token
}: BearerTokenInterface) => {
  return await fetchWithToken({
    url: RATING_BY_ID(rating?.id),
    params: {},
    method: Methods.GET,
    bearerToken: token,
  });
};


export const create = async({
  rating,
  token,
}: BearerTokenInterface): Promise<number> => {
  const response: Response = await fetchWithToken({
    url: CREATE_RATING,
    params: {},
    method: Methods.POST,
    data: rating,
    bearerToken: token,
  });

  return response.status;
};

export const update = async({
  rating,
  token,
}: BearerTokenInterface): Promise<number> => {
  const response: Response = await fetchWithToken({
    url: UPDATE_RATING(rating?.id),
    params: {},
    method: Methods.PUT,
    data: rating,
    bearerToken: token,
  });

  return response.status;
};


export const destroy = async({
  rating,
  token,
}: BearerTokenInterface): Promise<number> => {
  const response: Response = await fetchWithToken({
    url: DELETE_RATING(rating?.id),
    params: {},
    method: Methods.DELETE,
    bearerToken: token,
  });

  return response.status;
};
