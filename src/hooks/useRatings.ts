"use client"


import { useEffect, useState } from "react";
import { useAuthContext } from "./useAuthContext";
import { get, create, getById, update, destroy } from "@/api/ratings";
import { Rating } from "@/definations/interfaces/ratings";

export const useRatings = () => {
  const { state } = useAuthContext();
  const [ratings, setRatings] = useState<Rating[]>([]);

  useEffect(() => {
    get({ token: state.token.access }).then((ratings) => setRatings(ratings));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ratings]);
  
    
  const byId = (id: number) => {
    return getById({
      token: state.token.access,
      id: id,
    });
  };
  
  const createOrUpdate = (rating: Rating) => {
    if (rating.id) {
      return update({
        rating: rating,
        token: state.token.access,
      });
    } else {
      return create({ 
        rating: rating,
        token: state.token.access,
      })
    }
  };


  const softDelete = (rating: Rating) => {
    return destroy({
      rating: rating,
      token: state.token.access,
    })
  };
  


  if(!ratings) {
    return;
  }

  return { 
    ratings,
    byId,
    createOrUpdate,
    softDelete,
  };
}
