"use client"

import { useEffect, useState } from "react";
import { useAuthContext } from "./useAuthContext";
import { get, create, getById, update, destroy } from "@/api/movies";
import { Movie } from "@/definations/interfaces/movies";

export const useMovies = () => {
  const { state } = useAuthContext();
  const [movies, setMovies] = useState<Movie[]>([]);

  useEffect(() => {
    get().then((movies) => setMovies(movies));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [movies]);
  
  console.log(movies);
    
  const byId = (id: number) => {
    return getById({
      token: state.token.access,
      id: id,
    });
  };
  
  const createOrUpdate = (movie: Movie) => {
    if (movie.id) {
      return update({
        movie: movie,
        token: state.token.access,
      });
    } else {
      return create({ 
        movie: movie,
        token: state.token.access,
      })
    }
  };


  const softDelete = (movie: Movie) => {
    return destroy({
      movie: movie,
      token: state.token.access,
    })
  };
  


  if(!movies) {
    return;
  }

  return { 
    movies,
    byId,
    createOrUpdate,
    softDelete,
  };
}
