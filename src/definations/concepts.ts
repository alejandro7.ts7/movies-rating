export const mainMenu = [
  {
    label: 'SignIn',
    icon: 'pi pi-fw pi-sign-in',
  },
  {
    label: 'SignUp',
    icon: 'pi pi-user',
  },
  {
    label: 'Watchlist',
    icon: 'pi pi-list',
  },
  {
    label: 'Movies',
    icon: 'pi pi-video',
  },
  
];
