export const BACKEND_URL='http://127.0.0.1:8000/';

// auth endpoints
export const SIGININ=`${BACKEND_URL}/api/auth/signin/`;
export const SIGNUP=`${BACKEND_URL}/api/auth/signup/`;

// movies
export const CREATE_MOVIE = `${BACKEND_URL}/api/movies/create`;
export const MOVIES = `${BACKEND_URL}/api/movies/`;
export const MOVIE_BY_ID = (id: number | undefined) => `${BACKEND_URL}/api/movies/${id}`;
export const DELETE_MOVIE = (id: number | undefined) => `${BACKEND_URL}/api/movies/${id}/delete`;
export const UPDATE_MOVIE = (id: number | undefined) => `${BACKEND_URL}/api/movies/${id}/update`;


// ratings
export const CREATE_RATING = `${BACKEND_URL}/api/ratings/create`;
export const RATINGS = `${BACKEND_URL}/api/ratings/`;
export const RATING_BY_ID = (id: number | undefined) => `${BACKEND_URL}/api/ratings/${id}`;
export const DELETE_RATING = (id: number | undefined) => `${BACKEND_URL}/api/ratings/${id}/delete`;
export const UPDATE_RATING = (id: number | undefined) => `${BACKEND_URL}/api/ratings/${id}/update`;


// watchlist
export const ADD_MOVIE = `${BACKEND_URL}/api/watchlists/`;
export const WATCHLIST = `${BACKEND_URL}/api/watchlists/add`;
export const REMOVE_MOVIE = (id: number | undefined) => `${BACKEND_URL}/api/watchlists/${id}/remove`;
