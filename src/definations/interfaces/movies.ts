export interface Genre {
  id?: number;
  name: string;
}


export interface Movie {
  id?: number;
  title: string;
  release_date: string;
  rating_average?: number;
  genre_name?: string;
  plot: string;
  genre: number; 
};


export interface Watchlist {
  id?: number;
  user: number;
  movie: number;
}
