export interface UserInterface {
  username: string;
  password: string;
}

export interface RegisterUserInterface {
  username: string; 
  email: string; 
  firstName: string; 
  lastName: string; 
  passwordOne: string;
  passwordTwo: string;
  token: string;
}

export interface SignUpInterface {
  username: string; 
  email: string; 
  firstName: string; 
  lastName: string; 
  passwordOne: string;
  passwordTwo: string;
}
