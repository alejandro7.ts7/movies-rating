export interface Rating {
  id?: number;
  rating: number;
  comment: string;
  movie: number;
  user: number;
}
