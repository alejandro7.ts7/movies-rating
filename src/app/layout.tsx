import "primereact/resources/themes/lara-light-indigo/theme.css"; 
import 'primeflex/primeflex.css';
import "primereact/resources/primereact.min.css";
import 'primeicons/primeicons.css';
import { AuthContextProvider } from '@/context/authContext'
import { GlobalStyles } from '@/styles/Global';
import { Inter } from 'next/font/google'

const inter = Inter({ subsets: ['latin'] })

export const metadata = {
  title: 'Movies',
  description: 'Movies App',
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en">
      <body className={inter.className}>
        <AuthContextProvider>
          <GlobalStyles />
          {children}
        </AuthContextProvider>
      </body>
    </html>
  )
}
