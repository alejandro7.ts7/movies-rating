import MoviesList from "@/components/list";
import Navbar from "@/components/navbar";

export default function Home() {
  return (
    <main>
      <Navbar />
      <MoviesList />
    </main>
  )
}
