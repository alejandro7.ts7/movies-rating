# movies-rating

## Name
Movies Rating

## Installation
### Within Docker
- Open terminal
- Go to folder project
    - `cd movies-rating/`
- Run docker-compose
    - `docker-compose up`
- Go to **localhost:3000**

### Without Docker
- Install node js
- Install pnpm globaly
    - `npm install -g pnpm`
- Go to folder project
    - `cd movies-rating/`
- Run `pnpm install`
- Run `pnpm build`
- Run `pnpm start`

- Go to **localhost:8000**
